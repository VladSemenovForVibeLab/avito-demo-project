package ru.semenov.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@ContextConfiguration(classes = EmailConfiguration.class)
@ActiveProfiles("test")
public class EmailConfigurationTest {
    @Autowired
    private EmailConfiguration emailConfiguration;

    @Test
    void getJavaMailSender() {
        JavaMailSender javaMailSender = emailConfiguration.getJavaMailSender();
        assertThat(javaMailSender).isNotNull();
    }
}
