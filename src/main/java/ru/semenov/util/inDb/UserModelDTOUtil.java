package ru.semenov.util.inDb;

import org.springframework.stereotype.Component;
import ru.semenov.dto.userModel.UserModelDTORequestForRegister;
import ru.semenov.model.inDb.UserModel;
import ru.semenov.util.EntityDTOUtil;

import java.util.List;
@Component
public class UserModelDTOUtil implements EntityDTOUtil<UserModel, UserModelDTORequestForRegister> {

    @Override
    public UserModelDTORequestForRegister toDTO(UserModel element) {
        return new UserModelDTORequestForRegister(element);
    }

    @Override
    public UserModel toEntity(UserModelDTORequestForRegister dto) {
        return UserModel
                .builder()
                .email(dto.getEmail())
                .password(dto.getPassword())
                .accountExpired(true)
                .active(true)
                .block(true)
                .name(dto.getName())
                .phoneNumber(dto.getPhoneNumber())
                .build();
    }

    @Override
    public List<UserModelDTORequestForRegister> toDTO(List<UserModel> elements) {
        return elements.stream().map(this::toDTO).toList();
    }

    @Override
    public List<UserModel> toEntity(List<UserModelDTORequestForRegister> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }
}
