package ru.semenov.util.inDb;

import org.springframework.stereotype.Component;
import ru.semenov.dto.response.UserModelResponse;
import ru.semenov.model.inDb.UserModel;
import ru.semenov.util.EntityDTOUtil;

import java.util.List;
@Component
public class UserModelDTOUtilResponse implements EntityDTOUtil<UserModel, UserModelResponse> {
    @Override
    public UserModelResponse toDTO(UserModel element) {
        return new UserModelResponse(element);
    }

    @Override
    public UserModel toEntity(UserModelResponse dto) {
        return UserModel
                .builder()
                .phoneNumber(dto.getPhoneNumber())
                .email(dto.getEmail())
                .name(dto.getName())
                .imageModel(dto.getImageModel())
                .build();
    }

    @Override
    public List<UserModelResponse> toDTO(List<UserModel> elements) {
        return elements.stream().map(this::toDTO).toList();
    }

    @Override
    public List<UserModel> toEntity(List<UserModelResponse> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }
}
