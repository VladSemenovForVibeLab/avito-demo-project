package ru.semenov.util.inDb;

import org.springframework.stereotype.Component;
import ru.semenov.dto.imageModel.ImageModelDTO;
import ru.semenov.model.inDb.ImageModel;
import ru.semenov.util.EntityDTOUtil;

import java.util.List;


@Component
public class ImageModelDTOUtil implements EntityDTOUtil<ImageModel, ImageModelDTO> {
    @Override
    public ImageModelDTO toDTO(ImageModel element) {
        return new ImageModelDTO(element);
    }

    @Override
    public ImageModel toEntity(ImageModelDTO dto) {
        return ImageModel.builder()
                .isPreviewImage(dto.isPreviewImage())
                .contentType(dto.getContentType())
                .iid(dto.getIid())
                .name(dto.getName())
                .bytes(dto.getBytes())
                .size(dto.getSize())
                .originalFileName(dto.getOriginalFileName())
                .build();
    }

    @Override
    public List<ImageModelDTO> toDTO(List<ImageModel> elements) {
        return elements.stream().map(this::toDTO).toList();
    }

    @Override
    public List<ImageModel> toEntity(List<ImageModelDTO> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }
}
