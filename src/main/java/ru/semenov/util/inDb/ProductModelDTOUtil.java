package ru.semenov.util.inDb;

import org.springframework.stereotype.Component;
import ru.semenov.dto.productModel.ProductModelDTO;
import ru.semenov.model.inDb.ProductModel;
import ru.semenov.util.EntityDTOUtil;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductModelDTOUtil implements EntityDTOUtil<ProductModel, ProductModelDTO> {
    @Override
    public ProductModelDTO toDTO(ProductModel element) {
        return new ProductModelDTO(element);
    }

    @Override
    public ProductModel toEntity(ProductModelDTO dto) {
        return ProductModel
                .builder()
                .id(dto.getId())
                .url(dto.getUrl())
                .image(dto.getImage())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .price(dto.getPrice())
                .city(dto.getCity())
                .date(dto.getDate())
                .author(dto.getAuthor())
                .build();
    }

    @Override
    public List<ProductModelDTO> toDTO(List<ProductModel> elements) {
        return elements.stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<ProductModel> toEntity(List<ProductModelDTO> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }
}
