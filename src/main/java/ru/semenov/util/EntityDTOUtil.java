package ru.semenov.util;

import org.springframework.stereotype.Component;

import java.util.List;

public interface EntityDTOUtil<E,D> {
    public D toDTO(E element);
    public E toEntity(D dto);
    public List<D> toDTO(List<E> elements);
    public List<E> toEntity(List<D> dtos);
}
