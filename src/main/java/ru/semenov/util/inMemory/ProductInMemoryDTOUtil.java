package ru.semenov.util.inMemory;

import org.springframework.stereotype.Component;
import ru.semenov.dto.product.response.ProductDTO;
import ru.semenov.model.inMemory.Product;
import ru.semenov.util.EntityDTOUtil;

import java.util.List;
@Component
public class ProductInMemoryDTOUtil implements EntityDTOUtil<Product, ProductDTO> {
    @Override
    public ProductDTO toDTO(Product element) {
        return new ProductDTO(element);
    }

    @Override
    public Product toEntity(ProductDTO dto) {
        return Product
                .builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .price(dto.getPrice())
                .url(dto.getUrl())
                .city(dto.getCity())
                .date(dto.getDate())
                .image(dto.getImage())
                .author(dto.getAuthor())
                .build();
    }

    @Override
    public List<ProductDTO> toDTO(List<Product> elements) {
        return elements.stream().map(this::toDTO).toList();
    }

    @Override
    public List<Product> toEntity(List<ProductDTO> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }
}
