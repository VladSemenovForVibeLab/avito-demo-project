package ru.semenov.model.enums;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

public enum Role implements GrantedAuthority, Serializable {
    ROLE_ADMIN, ROLE_USER, ROLE_EXTERNAL, ROLE_SUPER_ADMIN, ROLE_SUPER_USER, ROLE_EMPLOYEE, ROLE_MANAGER;

    @Override
    public String getAuthority() {
        return name().toLowerCase();
    }
}
