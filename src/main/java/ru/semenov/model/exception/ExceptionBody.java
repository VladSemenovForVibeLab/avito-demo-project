package ru.semenov.model.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExceptionBody {
    private String message;
    private String code;
    private String status;
    private String path;
    private String timestamp;
    private String traceId;
    private String requestId;
    private String statusCode;
    private String error;

    public ExceptionBody(String message, String code) {
        this.message = message;
        this.code = code;
    }
    public ExceptionBody(String message, String code, String status) {
        this.message = message;
        this.code = code;
        this.status = status;
    }
    public ExceptionBody(String message, String code, String status, String path) {
        this.message = message;
        this.code = code;
        this.status = status;
        this.path = path;
    }
    public ExceptionBody(String message, String code, String status, String path, String timestamp) {
        this.message = message;
        this.code = code;
        this.status = status;
        this.path = path;
        this.timestamp = timestamp;
    }
    public ExceptionBody(String message, String code, String status, String path, String timestamp, String traceId) {
        this.message = message;
        this.code = code;
        this.status = status;
        this.path = path;
        this.timestamp = timestamp;
        this.traceId = traceId;
    }
    public ExceptionBody(String message, String code, String status, String path, String timestamp, String traceId, String requestId) {
        this.message = message;
        this.code = code;
        this.status = status;
        this.path = path;
        this.timestamp = timestamp;
        this.traceId = traceId;
        this.requestId = requestId;
    }
}
