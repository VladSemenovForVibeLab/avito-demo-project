package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Payment.TABLE_NAME)
@Data
@Builder
public class Payment implements Serializable,Comparable<Payment> {
    public static final String TABLE_NAME = "payment";
    private static final String PAYMENT_PID_COLUMN_NAME = "id";
    private static final String PAYMENT_AMOUNT_COLUMN_NAME = "amount";
    private static final String PAYMENT_DATE_COLUMN_NAME = "date";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = PAYMENT_PID_COLUMN_NAME, unique = true, nullable = false)
    private String pid;
    @Column(name = PAYMENT_AMOUNT_COLUMN_NAME)
    private BigDecimal amount;
    @Column(name = PAYMENT_DATE_COLUMN_NAME)
    private Date date;
    @ManyToOne
    @JoinColumn(name = "order_model_oid")
    private OrderModel orderModel;

    @Override
    public int compareTo(Payment o) {
        return this.date.compareTo(o.date);
    }
}
