package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.semenov.model.enums.Role;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = UserModel.TABLE_NAME)
public class UserModel implements Serializable, UserDetails {
    public static final String TABLE_NAME = "user_model";
    private static final String USER_MODEL_UID_COLUMN_NAME = "uid";
    private static final String USER_MODEL_EMAIL_COLUMN_NAME = "uemail";
    private static final String USER_MODEL_UPASSWORD_COLUMN_NAME = "upassword";
    private static final String USER_MODEL_PHONE_NUMBER_COLUMN_NAME = "uphone_number";
    private static final String USER_MODEL_NAME_COLUMN_NAME = "uname";
    private static final String USER_MODEL_ACTIVE_COLUMN_NAME = "active";
    private static final String USER_MODEL_EXPIRED_COLUMN_NAME = "expired";
    private static final String USER_MODEL_BLOCK_COLUMN_NAME = "block";
    private static final String USER_MODEL_ACCOUNT_EXPIRED_COLUMN_NAME = "account_expired";
    private static final String USER_MODEL_IMAGE_MODEL_COLUMN_NAME = "image_model_id";
    private static final String USER_MODEL_DATE_CREATED_COLUMN_NAME = "date_created";
    private static final String USER_MODEL_ACTIVE_CODE_COLUMN_NAME = "active_code";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = USER_MODEL_UID_COLUMN_NAME, unique = true, nullable = false)
    private String uid;
    @Column(name = USER_MODEL_EMAIL_COLUMN_NAME,unique = true,nullable = false,length = 1000)
    private String email;
    @Column(name = USER_MODEL_UPASSWORD_COLUMN_NAME,length = 10000)
    private String password;
    @Lob
    @Column(columnDefinition = "LONGBLOB")
    private byte[] bytes;
    @Column(name = USER_MODEL_PHONE_NUMBER_COLUMN_NAME)
    private String phoneNumber;
    @Column(name = USER_MODEL_NAME_COLUMN_NAME)
    private String name;
    @Column(name = USER_MODEL_ACTIVE_COLUMN_NAME)
    private boolean active=true;
    @Column(name = USER_MODEL_EXPIRED_COLUMN_NAME)
    private boolean expired=true;
    @Column(name = USER_MODEL_BLOCK_COLUMN_NAME)
    private boolean block=true;
    @Column(name = USER_MODEL_ACCOUNT_EXPIRED_COLUMN_NAME)
    private boolean accountExpired=true;
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = USER_MODEL_IMAGE_MODEL_COLUMN_NAME)
    private ImageModel imageModel;
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_model_uid"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles = new HashSet<>();
    @Column(name = USER_MODEL_DATE_CREATED_COLUMN_NAME)
    private LocalDateTime dateCreated;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "userModel")
    private List<ProductModel> productModels;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "userModel")
    private List<ReviewModel> reviewModels;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "userModel")
    private List<StoreModel> storeModels;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "department_model_did")
    private DepartmentModel departmentModel;
    @Column(name = USER_MODEL_ACTIVE_CODE_COLUMN_NAME)
    private String activeCode;
    @PrePersist
    private void init(){
        dateCreated=LocalDateTime.now();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return block;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return expired;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }


}
