package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = AddressModel.TABLE_NAME)
@Data
@Builder
public class AddressModel implements Serializable,Comparable<AddressModel> {
    public static final String TABLE_NAME = "address_model";
    private static final String ADDRESS_MODEL_AID_COLUMN_NAME = "aid";
    private static final String ADDRESS_MODEL_STREET_COLUMN_NAME = "street";
    private static final String ADDRESS_MODEL_CITY_COLUMN_NAME = "city";
    private static final String ADDRESS_MODEL_ZIP_CODE_COLUMN_NAME = "zip_code";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = ADDRESS_MODEL_AID_COLUMN_NAME, unique = true, nullable = false)
    private String aid;
    @Column(name = ADDRESS_MODEL_STREET_COLUMN_NAME)
    private String street;
    @Column(name = ADDRESS_MODEL_CITY_COLUMN_NAME)
    private String city;
    @Column(name = ADDRESS_MODEL_ZIP_CODE_COLUMN_NAME)
    private String zipCode;
    @ManyToOne
    @JoinColumn(name = "user_model_uid")
    private UserModel userModel;
    @ManyToOne
    @JoinColumn(name = "store_model_sid")
    private StoreModel storeModel;

    @Override
    public int compareTo(AddressModel o) {
        return  this.city.compareTo(o.city);
    }
}
