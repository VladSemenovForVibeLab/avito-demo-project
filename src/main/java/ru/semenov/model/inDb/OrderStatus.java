package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table(name = OrderStatus.TABLE_NAME)
public class OrderStatus implements Serializable,Comparable<OrderStatus> {
    public static final String TABLE_NAME = "order_status";
    private static final String ORDER_STATUS_OID_COLUMN_NAME = "oid";
    private static final String ORDER_STATUS_STATUS_COLUMN_NAME = "status";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = ORDER_STATUS_OID_COLUMN_NAME, unique = true, nullable = false)
    private String oid;
    @Column(name = ORDER_STATUS_STATUS_COLUMN_NAME)
    private String status;
    @OneToMany(mappedBy = "orderStatus", cascade = CascadeType.ALL)
    private Set<OrderModel> orders;

    @Override
    public int compareTo(OrderStatus o) {
        return this.status.compareTo(o.status);
    }
}
