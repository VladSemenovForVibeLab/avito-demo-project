package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = DepartmentModel.TABLE_NAME)
public class DepartmentModel implements Serializable,Comparable<DepartmentModel> {
    public static final String TABLE_NAME = "department_model";
    private static final String DEPARTMENT_MODEL_DID_COLUMN_NAME = "did";
    private static final String DEPARTMENT_MODEL_NAME_COLUMN_NAME = "name";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = DEPARTMENT_MODEL_DID_COLUMN_NAME, unique = true, nullable = false)
    private String did;
    @Column(name = DEPARTMENT_MODEL_NAME_COLUMN_NAME)
    private String name;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "departmentModel")
    private List<UserModel> employeeModels;

    @Override
    public int compareTo(DepartmentModel o) {
        return this.name.compareTo(o.name);
    }
}
