package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = ProductModel.PRODUCT_MODEL_TABLE_NAME)
public class ProductModel implements Serializable,Comparable<ProductModel> {
    public static final String PRODUCT_MODEL_TABLE_NAME="product_model";
    private static final String PRODUCT_MODEL_PID_COLUMN_NAME = "id";
    private static final String PRODUCT_MODEL_PTITLE_COLUMN_NAME = "title";
    private static final String PRODUCT_MODEL_PDESCRIPTION_COLUMN_NAME = "description";
    private static final String PRODUCT_MODEL_PPRICE_COLUMN_NAME="price";
    private static final String PRODUCT_MODEL_PCITY_COLUMN_NAME="city";
    private static final String PRODUCT_MODEL_PAUTHOR_COLUMN_NAME="author";
    private static final String PRODUCT_MODEL_PURL_COLUMN_NAME="url";
    private static final String PRODUCT_MODEL_PIMAGE_COLUMN_NAME="image";
    private static final String PRODUCT_MODEL_PDATE_COLUMN_NAME="date";
    private static final String PRODUCT_MODEL_PPREVIEWIMAGE_COLUMN_NAME="preview_image";
    private static final String PRODUCT_MODEL_PDATECREATED_COLUMN_NAME="date_created";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = PRODUCT_MODEL_PID_COLUMN_NAME,unique = true,nullable = false)
    private String id;
    @Column(name = PRODUCT_MODEL_PTITLE_COLUMN_NAME,unique = true,nullable = false)
    private String title;
    @Column(name = PRODUCT_MODEL_PDESCRIPTION_COLUMN_NAME,nullable = false,length = 10000, columnDefinition = "text")
    private String description;
    @Column(name = PRODUCT_MODEL_PPRICE_COLUMN_NAME,nullable = false)
    private int price;
    @Column(name = PRODUCT_MODEL_PCITY_COLUMN_NAME,nullable = false)
    private String city;
    @Column(name = PRODUCT_MODEL_PAUTHOR_COLUMN_NAME,nullable = false,length=100)
    private String author;
    @Column(name =PRODUCT_MODEL_PURL_COLUMN_NAME)
    private String url;
    @Column(name = PRODUCT_MODEL_PIMAGE_COLUMN_NAME)
    private String image;
    @Column(name = PRODUCT_MODEL_PDATE_COLUMN_NAME)
    private String date;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "productModel")
    private List<ImageModel> imageModels;
    @Column(name = PRODUCT_MODEL_PPREVIEWIMAGE_COLUMN_NAME)
    private String previewImageId;
    @Column(name = PRODUCT_MODEL_PDATECREATED_COLUMN_NAME)
    private LocalDateTime dateCreated;
    @ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinColumn
    private UserModel userModel;
    @ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinColumn
    private CategoryModel categoryModel;
    @ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinColumn
    private StoreModel storeModel;
    @OneToMany(cascade = CascadeType.REFRESH,fetch = FetchType.LAZY,mappedBy = "productModel")
    private List<ReviewModel> reviewModels;
    @PrePersist
    private void init(){
        dateCreated=LocalDateTime.now();
    }
    public void addImageToProduct(ImageModel imageModel){
        imageModel.setProductModel(this);
        imageModels.add(imageModel);
    }

    public ProductModel(String id,String title, String description, int price, String city, String author) {
        this.id=id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.city = city;
        this.author = author;
    }

    @Override
    public int compareTo(ProductModel o) {
        return this.dateCreated.compareTo(o.dateCreated);
    }
}
