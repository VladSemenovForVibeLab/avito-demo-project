package ru.semenov.model.inDb;

import jakarta.annotation.PreDestroy;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class StatusModel {
    private static final String STATUS_MODEL_TABLE_NAME="status_model";
    private static final String STATUS_MODEL_SID="sid";
    private static final String STATUS_MODEL_CONTENT="content";
    private static final String STATUS_MODEL_CREATED_AT="created_at";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Column(name = STATUS_MODEL_SID,unique = true,nullable = false)
    private String sid;
    @Column(name = STATUS_MODEL_CONTENT)
    private String content;
    @Column(name = STATUS_MODEL_CREATED_AT,nullable = false)
    private LocalDateTime createdAt;
    @PrePersist
    private void createdAt(){
        this.createdAt = LocalDateTime.now();
    }
    @PreDestroy
    public void destroy(){
        System.out.println("Destroy");
    }

}
