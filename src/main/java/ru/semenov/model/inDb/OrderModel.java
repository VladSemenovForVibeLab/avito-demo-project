package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = OrderModel.TABLE_NAME)
@Entity
@Builder
public class OrderModel implements Serializable, Comparable<OrderModel> {
    public static final String TABLE_NAME = "order_model";
    private static final String ORDER_MODEL_PID_COLUMN_NAME = "id";
    private static final String ORDER_MODEL_DATE_COLUMN_NAME = "date";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = ORDER_MODEL_PID_COLUMN_NAME, unique = true, nullable = false)
    private String id;
    @Column(name = ORDER_MODEL_DATE_COLUMN_NAME)
    private Date date;
    @ManyToOne
    @JoinColumn(name = "user_model_uid")
    private UserModel userModel;
    @ManyToOne
    @JoinColumn(name = "order_status_oid")
    private OrderStatus orderStatus;
    @OneToMany(mappedBy = "orderModel", cascade = CascadeType.ALL)
    private Set<Payment> payments;
    @OneToOne
    @JoinColumn
    private DeliveryInfo deliveryInfo;
    @ManyToOne
    @JoinColumn(name = "store_model_sid")
    private StoreModel storeModel;

    @Override
    public int compareTo(OrderModel o) {
        return this.date.compareTo(o.date);
    }
}
