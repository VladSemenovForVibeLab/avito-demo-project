package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import ru.semenov.model.inMemory.Product;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = CategoryModel.CATEGORY_MODEL_TABLE_NAME)
@Entity
public class CategoryModel implements Serializable,Comparable<CategoryModel> {
    public static final String CATEGORY_MODEL_TABLE_NAME="category_model";
    private static final String CATEGORY_MODEL_ID_COLUMN_NAME="id";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = CATEGORY_MODEL_ID_COLUMN_NAME)
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "created_at")
    private String createdAt;
    @Column(name = "updated_at")
    private String updatedAt;
    @Column(name = "deleted_at")
    private String deletedAt;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "deleted_by")
    private String deletedBy;
    @Column(name = "version")
    private Integer version;
    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    @Column(name = "is_locked")
    private Boolean isLocked;
    @Column(name = "is_published")
    private Boolean isPublished;
    @OneToMany(mappedBy = "categoryModel", cascade = CascadeType.ALL)
    private List<ProductModel> productModels;
    @ManyToOne
    @JoinColumn(name = "store_model_sid")
    private StoreModel storeModel;

    @Override
    public int compareTo(CategoryModel o) {
        return this.getName().compareTo(o.getName());
    }
}
