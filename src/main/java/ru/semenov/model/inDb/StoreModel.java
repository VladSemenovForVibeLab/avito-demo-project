package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
@Data
public class StoreModel implements Serializable,Comparable<StoreModel> {
    private static final String STORE_MODEL_STORE_ID_COLUMN_NAME="sid";
    private static final String STORE_MODEL_NAME_COLUMN_NAME="name";
    private static final String STORE_MODEL_LOCATION_COLUMN_NAME="location";
    private static final String STORE_MODEL_ACTIVE_COLUMN_NAME="active";
    private static final String STORE_MODEL_NUMBER_OF_PRODUCTS_COLUMN_NAME="number_of_products";
    private static final String STORE_MODEL_TAG_COLUMN_NAME="stag";


    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = STORE_MODEL_STORE_ID_COLUMN_NAME, unique = true, nullable = false)
    private String sid;
    @Column(name = STORE_MODEL_NAME_COLUMN_NAME, nullable = false)
    private String name;
    @Column(name = STORE_MODEL_LOCATION_COLUMN_NAME)
    private String location;
    @Column(name = STORE_MODEL_ACTIVE_COLUMN_NAME)
    private boolean active;
    @Column(name = STORE_MODEL_NUMBER_OF_PRODUCTS_COLUMN_NAME)
    private int numberOfProducts;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "user_model_uid")
    private UserModel userModel;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "storeModel")
    private List<ProductModel> productModels;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "storeModel")
    private List<ReviewModel> reviewModels;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "storeModel")
    private List<ImageModel> imageModels;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "storeModel")
    private List<OrderModel> orderModels;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "storeModel")
    private List<AddressModel> addressModels;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "storeModel")
    private List<CategoryModel> categoryModels;
    private Date createdAt;
    @Column(name = STORE_MODEL_TAG_COLUMN_NAME)
    private String tag;

    @PrePersist
    public void prePersist() {
        this.createdAt = new Date();
    }
    @Override
    public int compareTo(StoreModel o) {
        return Integer.compare(this.numberOfProducts,o.numberOfProducts);
    }
}
