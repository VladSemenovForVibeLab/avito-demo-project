package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Data
@Table(name = DeliveryInfo.TABLE_NAME)

public class DeliveryInfo implements Serializable,Comparable<DeliveryInfo> {
    public static final String TABLE_NAME = "delivery_info";
    private static final String DELIVERY_INFO_DID_COLUMN_NAME = "did";
    private static final String DELIVERY_INFO_DELIVERY_ADDRESS_COLUMN_NAME = "delivery_address";
    private static final String DELIVERY_INFO_DELIVERY_DATE_COLUMN_NAME = "delivery_date";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = DELIVERY_INFO_DID_COLUMN_NAME, unique = true, nullable = false)
    private String did;
    @Column(name = DELIVERY_INFO_DELIVERY_ADDRESS_COLUMN_NAME)
    private String deliveryAddress;
    @Column(name = DELIVERY_INFO_DELIVERY_DATE_COLUMN_NAME)
    private Date deliveryDate;
    @OneToOne(cascade = CascadeType.ALL,mappedBy = "deliveryInfo")
    private  OrderModel orderModel;

    @Override
    public int compareTo(DeliveryInfo o) {
        return this.deliveryAddress.compareTo(o.deliveryAddress);
    }
}
