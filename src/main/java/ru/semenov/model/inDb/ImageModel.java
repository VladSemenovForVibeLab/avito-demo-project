package ru.semenov.model.inDb;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = ImageModel.TABLE_NAME)
public class ImageModel implements Serializable,Comparable<ImageModel> {

    public static final String TABLE_NAME = "image_model";
    private static final String IMAGE_MODEL_IID_COLUMN_NAME = "iid";
    private static final String IMAGE_MODEL_INAME_COLUMN_NAME = "iname";
    private static final String IMAGE_MODEL_IORIGINAL_FILE_NAME_COLUMN_NAME = "ioriginal_file_name";
    private static final String IMAGE_MODEL_ISIZE_COLUMN_NAME = "isize";
    private static final String IMAGE_MODEL_IPREVIEW_IMAGE_COLUMN_NAME = "ipreview_image";
    private static final String IMAGE_MODEL_CONTENT_TYPE_COLUMN_NAME = "icontent_type";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = IMAGE_MODEL_IID_COLUMN_NAME, unique = true, nullable = false)
    private String iid;
    @Column(name = IMAGE_MODEL_INAME_COLUMN_NAME)
    private String name;
    @Column(name = IMAGE_MODEL_IORIGINAL_FILE_NAME_COLUMN_NAME)
    private String originalFileName;
    @Column(name = IMAGE_MODEL_ISIZE_COLUMN_NAME)
    private Long size;
    @Column(name = IMAGE_MODEL_IPREVIEW_IMAGE_COLUMN_NAME)
    private boolean isPreviewImage;
    @Column(name = IMAGE_MODEL_CONTENT_TYPE_COLUMN_NAME)
    private String contentType;
    @Lob
    @Column(columnDefinition = "LONGBLOB")
    private byte[] bytes;
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.EAGER)
    private ProductModel productModel;
    @ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.EAGER)
    private StoreModel storeModel;

    @Override
    public int compareTo(ImageModel o) {
        return this.name.compareTo(o.name);
    }
}
