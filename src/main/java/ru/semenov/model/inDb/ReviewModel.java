package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;

@Entity
@Table(name = ReviewModel.REVIEW_MODEL_TABLE_NAME)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ReviewModel implements Serializable,Comparable<ReviewModel> {
    public static final String REVIEW_MODEL_TABLE_NAME="review_model";
    private static final String REVIEW_MODEL_REVIEW_ID_COLUMN_NAME="review_id";
    private static final String REVIEW_MODEL_COMMENT_COLUMN_NAME="comment";
    private static final String REVIEW_MODEL_RATING_COLUMN_NAME="rating";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = REVIEW_MODEL_REVIEW_ID_COLUMN_NAME, unique = true, nullable = false)
    private String reviewId;
    @Column(name = REVIEW_MODEL_COMMENT_COLUMN_NAME)
    private String comment;
    @Column(name = REVIEW_MODEL_RATING_COLUMN_NAME)
    private int rating;
    @ManyToOne
    @JoinColumn(name = "product_model_pid")
    private ProductModel productModel;
    @ManyToOne
    @JoinColumn(name = "user_model_uid")
    private UserModel userModel;
    @ManyToOne
    @JoinColumn(name = "store_model_sid")
    private StoreModel storeModel;

    @Override
    public int compareTo(ReviewModel o) {
        return Integer.compare(this.rating,o.rating);
    }
}
