package ru.semenov.model.inDb;

import java.sql.Time;
import java.time.LocalDateTime;

public record CachePOJO(String id, LocalDateTime localDateTime) {
}
