package ru.semenov.model.inDb

data class Book (
        val Id: Int,
        val name: String,
        val authorId: Int,
)