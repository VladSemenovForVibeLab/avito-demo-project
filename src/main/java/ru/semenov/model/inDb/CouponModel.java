package ru.semenov.model.inDb;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = CouponModel.TABLE_NAME)
@Data
@Builder
public class CouponModel implements Serializable,Comparable<CouponModel> {
    public static final String TABLE_NAME = "coupon_model";
    private static final String COUPON_MODEL_CID_COLUMN_NAME = "cid";
    private static final String COUPON_MODEL_CODE_COLUMN_NAME = "code";
    private static final String COUPON_MODEL_DISCOUNT_COLUMN_NAME = "discount";
    private static final String COUPON_MODEL_DATE_CREATED_COLUMN_NAME = "date_created";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = COUPON_MODEL_CID_COLUMN_NAME, unique = true, nullable = false)
    private String cid;
    @Column(name = COUPON_MODEL_CODE_COLUMN_NAME)
    private String code;
    @Column(name = COUPON_MODEL_DISCOUNT_COLUMN_NAME)
    private BigDecimal discount;
    @Column(name = COUPON_MODEL_DATE_CREATED_COLUMN_NAME)
    private Date dateCreated;

    @Override
    public int compareTo(CouponModel o) {
        return this.discount.compareTo(o.discount);
    }
}
