package ru.semenov.model.inDb

data class Author(
        val Id: Int,
        val firstName: String,
        val lastName: String
)