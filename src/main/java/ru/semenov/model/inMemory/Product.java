package ru.semenov.model.inMemory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Product {
    private String title;
    private String description;
    private int price;
    private String city;
    private String author;
    private String url;
    private String image;
    private String date;
    private Long id;

    public Product(Long id,String title, String description, int price, String city, String author) {
        this.id=id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.city = city;
        this.author = author;
    }
}
