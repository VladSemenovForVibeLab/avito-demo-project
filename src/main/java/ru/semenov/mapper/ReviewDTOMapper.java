package ru.semenov.mapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.semenov.dto.record.review.ReviewDTO;
import ru.semenov.model.inDb.ReviewModel;

import java.util.function.Function;
@Service
@Slf4j
public class ReviewDTOMapper implements Function<ReviewModel, ReviewDTO> {
    @Override
    public ReviewDTO apply(ReviewModel reviewModel) {
        log.debug(" ReviewModel is " + reviewModel);
        return  new ReviewDTO(
                reviewModel.getReviewId(),
                reviewModel.getComment(),
                reviewModel.getRating(),
                reviewModel.getProductModel().getId(),
                reviewModel.getUserModel().getUid(),
                reviewModel.getStoreModel().getSid()
        );
    }
}
