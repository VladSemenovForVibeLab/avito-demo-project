package ru.semenov.chat;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChatNotification {
    private static final String ID = "id";
    private static final String SENDER_ID = "senderId";
    private static final String SENDER_NAME = "senderName";
    @Column(name = ID)
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    private String id;
    @Column(name = SENDER_ID)
    private String senderId;
    @Column(name = SENDER_NAME)
    private String senderName;
}
