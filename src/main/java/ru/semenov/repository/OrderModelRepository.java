package ru.semenov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.model.inDb.OrderModel;

@Repository
@RepositoryRestResource
public interface OrderModelRepository extends JpaRepository<OrderModel, String> {
}