package ru.semenov.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.model.inDb.StoreModel;
import ru.semenov.model.inDb.UserModel;

import java.util.Optional;

@Repository
@RepositoryRestResource
public interface StoreModelRepository extends JpaRepository<StoreModel, String> {
    Page<StoreModel> findAllByActive(boolean active, Pageable pageable);
    Page<StoreModel> findAllByTag(String tag, Pageable pageable);
    Page<StoreModel> findAllByTagAndActive(String tag, boolean active, Pageable pageable);
    Page<StoreModel> findAllByNameContaining(String name, Pageable pageable);
    Page<StoreModel> findAllByNameContainingAndActive(String name, boolean active, Pageable pageable);
    Page<StoreModel> findAllByLocationContaining(String location, Pageable pageable);
    Page<StoreModel> findAllByLocationContainingAndActive(String location, boolean active, Pageable pageable);
    Page<StoreModel> findAllByNumberOfProducts(int numberOfProducts, Pageable pageable);
    Page<StoreModel> findAllByNumberOfProductsAndActive(int numberOfProducts, boolean active, Pageable pageable);
    Page<StoreModel> findAllByUserModel(UserModel userModel, Pageable pageable);
}