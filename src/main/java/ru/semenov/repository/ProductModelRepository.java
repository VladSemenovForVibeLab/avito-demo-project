package ru.semenov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.model.inDb.ProductModel;

import java.util.Optional;

@Repository
@RepositoryRestResource
public interface ProductModelRepository extends JpaRepository<ProductModel,String> {
    Optional<ProductModel> findProductModelByTitle(String title);
    Optional<ProductModel> findProductModelByPrice(int price);
}
