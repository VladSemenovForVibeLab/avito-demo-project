package ru.semenov.repository;

import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.model.inDb.UserModel;

import java.awt.dnd.DropTarget;
import java.util.Optional;

@Repository
@RepositoryRestResource
public interface UserModelRepository extends JpaRepository<UserModel, String> {
    UserModel findUserModelByEmail(String email);
    UserModel findUserModelByUid(String uid);
    UserModel findUserModelByPhoneNumber(String phoneNumber);
    UserModel findUserModelByName(String name);

    Optional<UserModel> findUserModelByActiveCode(String code);
}