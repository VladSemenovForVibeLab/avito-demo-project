package ru.semenov.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.model.inDb.ProductModel;
import ru.semenov.model.inDb.ReviewModel;
import ru.semenov.model.inDb.StoreModel;
import ru.semenov.model.inDb.UserModel;

import java.util.List;

@Repository
@RepositoryRestResource
public interface ReviewModelRepository extends JpaRepository<ReviewModel, String> {
    List<ReviewModel> findAllByCommentContaining(String comment,Pageable pageable);
    List<ReviewModel> findAllByRatingContaining(int rating,Pageable pageable);

    List<ReviewModel> findAllByProductModel(ProductModel productModel, Pageable pageable);
    List<ReviewModel> findAllByUserModel(UserModel userModel, Pageable pageable);
    List<ReviewModel> findAllByStoreModel(StoreModel storeModel, Pageable pageable);
}