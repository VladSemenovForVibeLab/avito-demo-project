package ru.semenov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.model.inDb.DepartmentModel;
@Repository
@RepositoryRestResource
public interface DepartmentModelRepository extends JpaRepository<DepartmentModel, String> {
}