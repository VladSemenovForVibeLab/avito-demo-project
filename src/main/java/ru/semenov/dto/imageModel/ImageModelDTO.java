package ru.semenov.dto.imageModel;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.model.inDb.ImageModel;
import ru.semenov.model.inDb.ProductModel;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ImageModelDTO {
    private String iid;
    private String name;
    private String originalFileName;
    private Long size;
    private String contentType;
    private boolean isPreviewImage;
    @Lob
    private byte[] bytes;
    private ProductModel productModel;
    public ImageModelDTO(ImageModel imageModel){
        this.iid = imageModel.getIid();
        this.name = imageModel.getName();
        this.originalFileName = imageModel.getOriginalFileName();
        this.size = imageModel.getSize();
        this.isPreviewImage = imageModel.isPreviewImage();
        this.bytes = imageModel.getBytes();
        this.contentType = imageModel.getContentType();
        this.productModel = imageModel.getProductModel();
    }
}
