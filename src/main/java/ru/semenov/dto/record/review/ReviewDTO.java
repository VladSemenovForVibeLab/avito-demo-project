package ru.semenov.dto.record.review;

import ru.semenov.model.inDb.ProductModel;
import ru.semenov.model.inDb.StoreModel;
import ru.semenov.model.inDb.UserModel;

public record ReviewDTO(
        String reviewId,
        String comment,
        int rating,

        String productModelId,

        String userModelId,

        String storeModelId
) {

}
