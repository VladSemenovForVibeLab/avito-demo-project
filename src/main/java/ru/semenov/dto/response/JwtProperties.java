package ru.semenov.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "security.jwt")
public class JwtProperties {
    @Schema(description = "secret",example = "IT IS SECRET -_-")
    private String secret;
    @Schema(description = "access",example = "IT IS ACCESS -_-")
    private long access;
}


