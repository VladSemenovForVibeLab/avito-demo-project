package ru.semenov.dto.product.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.model.inMemory.Product;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProductDTO {
    private String title;
    private String description;
    private int price;
    private String city;
    private String author;
    private String url;
    private String image;
    private String date;
    private Long id;
    public ProductDTO(Long id,String title, String description, int price, String city, String author) {
        this.id=id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.city = city;
        this.author = author;
    }
    public ProductDTO(Product product) {
        this.id=product.getId();
        this.title = product.getTitle();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.city = product.getCity();
        this.author = product.getAuthor();
        this.url = product.getUrl();
        this.image = product.getImage();
        this.date = product.getDate();
    }
}
