package ru.semenov.dto.product.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductDTORequest {
    private String title;
    private String description;
    private int price;
    private String city;
    private String author;
    private String url;
    private String image;
    private String date;

    public ProductDTORequest(String title, String description, int price, String city, String author) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.city = city;
        this.author = author;
    }
}
