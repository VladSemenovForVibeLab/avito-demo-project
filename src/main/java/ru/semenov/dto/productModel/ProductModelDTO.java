package ru.semenov.dto.productModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.model.inDb.ProductModel;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProductModelDTO {
        private String title;
        private String description;
        private int price;
        private String city;
        private String author;
        private String url;
        private String image;
        private String date;
        private String id;
        public ProductModelDTO(ProductModel productModel){
                this.title = productModel.getTitle();
                this.description = productModel.getDescription();
                this.price = productModel.getPrice();
                this.city = productModel.getCity();
                this.author = productModel.getAuthor();
                this.url = productModel.getUrl();
                this.image = productModel.getImage();
                this.date = productModel.getDate();
                this.id = productModel.getId();
        }
}
