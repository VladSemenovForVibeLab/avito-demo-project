package ru.semenov.service.inMemory.impl;

import org.springframework.stereotype.Service;
import ru.semenov.dto.product.request.ProductDTORequest;
import ru.semenov.dto.product.response.ProductDTO;
import ru.semenov.model.exception.ResourceNotFoundException;
import ru.semenov.model.inMemory.Product;
import ru.semenov.service.inMemory.interf.IProductService;
import ru.semenov.util.inMemory.ProductInMemoryDTOUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class ProductServiceImpl implements IProductService {
    private final ProductInMemoryDTOUtil productInMemoryDTOUtil;
    public ProductServiceImpl(ProductInMemoryDTOUtil productInMemoryDTOUtil) {
        this.productInMemoryDTOUtil = productInMemoryDTOUtil;
    }
    private List<Product> products = new ArrayList<>();
    private long ID = 0;
    {
        products.add(new Product(++ID,"Радар", "Связь с меня в т", 1300, "Москва", "Александр"));
        products.add(new Product(++ID,"Сканер", "Связь с меня в т", 2700, "Москва", "Александр"));
        products.add(  new Product(++ID,"Дроид", "Связь с меня в т", 400, "Москва", "Александр"));
    }
    @Override
    public List<Product> list(){
        return products;
    }

    @Override
    public void saveProduct(ProductDTO productDTO) {
        productDTO.setId(++ID);
        products.add(productInMemoryDTOUtil.toEntity(productDTO));
    }

    @Override
    public ProductDTO read(Long id) {
        Stream<Product> productStream = products.stream().filter(product -> product.getId().equals(id));
        return productInMemoryDTOUtil
                .toDTO(productStream.findFirst().orElseThrow(()-> new ResourceNotFoundException("This product not found")));
    }

    @Override
    public void saveProduct(Product product){
        product.setId(++ID);
        products.add(product);
    }
    @Override
    public void deleteProduct(Long productId){
        products.removeIf(product->product.getId().equals(productId));
    }

    @Override
    public void updateProduct(ProductDTORequest productDTORequest, Long id) {
        Product productFind = getProductById(id);
        ProductDTO productUpdate = setProduct(productDTORequest, productFind);
    }

    @Override
    public Product getProductById(Long id) {
        return products.stream().filter(product -> product.getId().equals(id)).findFirst().orElseThrow(
                ()->new ResourceNotFoundException("This product not found!"));
    }
    public ProductDTO setProduct(ProductDTORequest productDTORequest, Product product) {
        product.setCity(productDTORequest.getCity());
        product.setDate(productDTORequest.getDate());
        product.setImage(productDTORequest.getImage());
        product.setAuthor(productDTORequest.getAuthor());
        product.setPrice(productDTORequest.getPrice());
        product.setTitle(productDTORequest.getTitle());
        product.setUrl(productDTORequest.getUrl());
        product.setDescription(productDTORequest.getDescription());
        return productInMemoryDTOUtil.toDTO(product);
    }
}
