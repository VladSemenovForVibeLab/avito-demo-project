package ru.semenov.service.inMemory.interf;

import ru.semenov.dto.product.request.ProductDTORequest;
import ru.semenov.dto.product.response.ProductDTO;
import ru.semenov.model.inMemory.Product;

import java.util.List;

public interface IProductService {
    List<Product> list();

    void saveProduct(ProductDTO productDTO);
    void saveProduct(Product product);

    ProductDTO read(Long id);

    void deleteProduct(Long id);

    void updateProduct(ProductDTORequest productDTORequest, Long id);
    Product getProductById(Long id);
}
