package ru.semenov.service.inDb.rest.interf;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.semenov.model.inDb.StoreModel;
import ru.semenov.model.inDb.UserModel;

import java.security.Principal;

public interface IStoreModelService {

    Page<StoreModel> findAllByActive(boolean active, Pageable pageable);

    Page<StoreModel> findAllByTag(String tag, Pageable pageable);

    Page<StoreModel> findAllByTagAndActive(String tag, boolean active, Pageable pageable);

    Page<StoreModel> findAllByNameContaining(String name, Pageable pageable);

    Page<StoreModel> findAllByNameContainingAndActive(String name, boolean active, Pageable pageable);

    Page<StoreModel> findAllByLocationContaining(String location, Pageable pageable);

    Page<StoreModel> findAllByLocationContainingAndActive(String location, boolean active, Pageable pageable);

    Page<StoreModel> findAllByNumberOfProducts(int numberOfProducts, Pageable pageable);

    Page<StoreModel> findAllByNumberOfProductsAndActive(int numberOfProducts, boolean active, Pageable pageable);

    Page<StoreModel> findAllByUserModel(Principal userModel, Pageable pageable);
}
