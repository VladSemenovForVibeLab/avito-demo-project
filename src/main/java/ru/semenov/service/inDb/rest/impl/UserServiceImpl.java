package ru.semenov.service.inDb.rest.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.request.JWTRequest;
import ru.semenov.dto.response.JWTResponse;
import ru.semenov.dto.response.MessageResponse;
import ru.semenov.dto.response.UserModelResponse;
import ru.semenov.dto.userModel.UserModelDTORequestForRegister;
import ru.semenov.model.enums.Role;
import ru.semenov.model.exception.ResourceNotFoundException;
import ru.semenov.model.inDb.UserModel;
import ru.semenov.repository.UserModelRepository;
import ru.semenov.security.JwtTokenProvider;
import ru.semenov.service.inDb.rest.interf.IMailSender;
import ru.semenov.service.inDb.rest.interf.IUserService;
import ru.semenov.util.inDb.UserModelDTOUtil;
import ru.semenov.util.inDb.UserModelDTOUtilResponse;

import java.util.UUID;

@Service
@Slf4j
public class UserServiceImpl implements IUserService {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserModelRepository userModelRepository;
    private final UserModelDTOUtil userModelDTOUtil;
    private final UserModelDTOUtilResponse userModelDTOUtilResponse;
    private final PasswordEncoder passwordEncoder;
    private final IMailSender mailSender;

    public UserServiceImpl(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserModelRepository userModelRepository, UserModelDTOUtil userModelDTOUtil, UserModelDTOUtilResponse userModelDTOUtilResponse, PasswordEncoder passwordEncoder, IMailSender mailSender) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userModelRepository = userModelRepository;
        this.userModelDTOUtil = userModelDTOUtil;
        this.userModelDTOUtilResponse = userModelDTOUtilResponse;
        this.passwordEncoder = passwordEncoder;
        this.mailSender = mailSender;
    }

    @Override
    @Transactional
    public boolean createUserModel(UserModelDTORequestForRegister userModelDTORequestForRegister) {
        if(userModelRepository.findUserModelByEmail(userModelDTORequestForRegister.getEmail())!= null) {
            log.error("User with email {} already exists", userModelDTORequestForRegister.getEmail());
        }
        toUserModelForRegister(userModelDTORequestForRegister);
        log.info("User with email {} created", userModelDTORequestForRegister.getEmail());
        return true;
    }

    @Override
    public UserModel getByEmail(String email) {
        return userModelRepository.findUserModelByEmail(email);
    }

    @Override
    public UserModelDTORequestForRegister findById(String userId) {
        return userModelDTOUtil.toDTO(
                userModelRepository.findUserModelByUid(userId)
        );
    }

    @Override
    public JWTResponse login(JWTRequest loginRequest) {
        JWTResponse jwtResponse = new JWTResponse();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),loginRequest.getPassword()));
        UserModel user = userModelRepository.findUserModelByEmail(loginRequest.getEmail());
        jwtResponse.setId(user.getUid());
        jwtResponse.setEmail(user.getEmail());
        jwtResponse.setName(user.getName());
        jwtResponse.setAccessToken(jwtTokenProvider.createAccessToken(user.getUid(),user.getEmail(),user.getRoles()));
        return jwtResponse;
    }

    @Override
    public UserModelResponse userModelProfile(String id) {
        return userModelDTOUtilResponse.toDTO(
                userModelRepository.findUserModelByUid(id)
        );
    }

    @Override
    @Transactional
    public MessageResponse adminProfile(String id) {
        UserModel userModel = userModelRepository.findUserModelByUid(id);
        userModel.getRoles().add(Role.ROLE_ADMIN);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"ADMIN");
    }

    @Override
    @Transactional
    public MessageResponse adminEnabled(String id) {
        UserModel userModel = userModelRepository.findUserModelByUid(id);
        userModel.setActive(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"ACTIVE -> FALSE");
    }

    @Override
    @Transactional
    public MessageResponse adminCredentialsExpired(String uid) {
        UserModel userModel = userModelRepository.findUserModelByUid(uid);
        userModel.setExpired(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"CREDENTIALS EXPIRED -> FALSE");
    }

    @Override
    @Transactional
    public MessageResponse adminAccountLocked(String uid) {
        UserModel userModel = userModelRepository.findUserModelByUid(uid);
        userModel.setBlock(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"CREDENTIALS EXPIRED -> FALSE");
    }

    @Override
    @Transactional
    public MessageResponse adminAccountExpired(String uid) {
        UserModel userModel = userModelRepository.findUserModelByUid(uid);
        userModel.setAccountExpired(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"ACCOUNT EXPIRED -> FALSE");
    }

    @Override
    public boolean activateUserModel(String code) {
        UserModel userModel = userModelRepository.findUserModelByActiveCode(code)
                .orElseThrow(
                        () -> new ResourceNotFoundException("User model not found!"));
        userModel.setActive(true);
        userModel.setActiveCode(null);
        userModelRepository.save(userModel);
        return true;
    }


    public void toUserModelForRegister(UserModelDTORequestForRegister userModelDTORequestForRegister) {
        UserModel userModel = new UserModel();
        userModel.setEmail(userModelDTORequestForRegister.getEmail());
        userModel.setPassword(passwordEncoder.encode(userModelDTORequestForRegister.getPassword()));
        userModel.setPhoneNumber(userModelDTORequestForRegister.getPhoneNumber());
        userModel.setName(userModelDTORequestForRegister.getName());
        userModel.getRoles().add(Role.ROLE_USER);
        userModel.setActive(false);
        userModel.setBlock(true);
        userModel.setExpired(true);
        userModel.setAccountExpired(true);
        userModel.setActiveCode(UUID.randomUUID().toString());
        userModelRepository.save(userModel);
        if(!StringUtils.isEmpty(userModel.getEmail())){
            String message = String.format(
                    "Hello, %s! \n"+
                            "Welcome to  \n"+
                            "Please, visit next link: http://localhost:8083/api/v1/rest/users/activate/%s",
                    userModel.getName(),
                    userModel.getActiveCode()
            );
            mailSender.sendMail(userModel.getEmail(),"Activation code",message);
        }
    }
}
