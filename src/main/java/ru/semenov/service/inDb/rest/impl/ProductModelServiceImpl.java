package ru.semenov.service.inDb.rest.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.semenov.dto.imageModel.ImageModelDTO;
import ru.semenov.dto.productModel.ProductModelDTO;
import ru.semenov.model.exception.ResourceNotFoundException;
import ru.semenov.model.inDb.ImageModel;
import ru.semenov.model.inDb.ProductModel;
import ru.semenov.model.inDb.UserModel;
import ru.semenov.repository.ProductModelRepository;
import ru.semenov.service.inDb.rest.interf.IProductModelService;
import ru.semenov.service.inDb.rest.interf.IUserService;
import ru.semenov.util.inDb.ProductModelDTOUtil;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Service
@Slf4j
public class ProductModelServiceImpl implements IProductModelService {
    private final ProductModelRepository productModelRepository;
    private final ProductModelDTOUtil productModelDTOUtil;
    private final IUserService userService;

    public ProductModelServiceImpl(ProductModelRepository productModelRepository, ProductModelDTOUtil productModelDTOUtil, IUserService userService) {
        this.productModelRepository = productModelRepository;
        this.productModelDTOUtil = productModelDTOUtil;
        this.userService = userService;
    }
    private ProductModel toUserModelForProductModel(Principal principal,ProductModel productModel){
        productModel.setUserModel(getUserModelByPrincipal(principal));
        return productModel;
    }

    private UserModel getUserModelByPrincipal(Principal principal) {
        if(principal==null){
            throw new IllegalArgumentException("User principal is null");
        }
        return userService.getByEmail(principal.getName());
    }

    @Override
    @Transactional
    public ProductModelDTO create(ProductModelDTO productModelDTO,Principal principal) {
        log.info("Create productModelDTO: {}", productModelDTO);
        ProductModel userModelForProductModel = toUserModelForProductModel(principal, productModelDTOUtil.toEntity(productModelDTO));
        return productModelDTOUtil.toDTO(productModelRepository.save(userModelForProductModel));
    }

    @Override
    @Transactional(readOnly = true)
    public ProductModelDTO read(String id) {
        log.info("Read");
        return productModelDTOUtil.toDTO(
                productModelRepository.findById(id).orElseThrow(
                        () -> new ResourceNotFoundException("This product not found!"))
        );
    }

    @Override
    @Transactional
    public ProductModelDTO update(ProductModelDTO productModelDTO, String id) {
        log.info("Update product: {}", productModelDTO);
        ProductModel entity = productModelRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("This product not found!"));
        return setProductModel(entity, productModelDTO);
    }

    @Override
    @Transactional
    public void delete(String id) {
        log.info("Delete product: {}", id);
        productModelRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductModelDTO> findAll() {
        log.info("Find all products");
        return productModelDTOUtil.toDTO(productModelRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public ProductModelDTO findByTitle(String title) {
        log.info("Find product by title");
        return productModelDTOUtil.toDTO(
                productModelRepository.findProductModelByTitle(title).orElseThrow(
                        () -> new ResourceNotFoundException("This product not found by this title!"))
        );
    }

    @Override
    @Transactional
    public ProductModelDTO saveImageForProductModel(MultipartFile image1, MultipartFile image2, MultipartFile image3, String id) throws IOException {
        log.info("Save image for productModel: {}", id);
        ProductModel model = findProductModelByProductModelId(id);
        ImageModel addImageModelForProduct = toAddImageModelForProduct(model, image1);
        addImageModelForProduct.setPreviewImage(true);
        toAddImageModelForProduct(model,image2);
        toAddImageModelForProduct(model,image3);
        ProductModel save = productModelRepository.save(model);
        save.setPreviewImageId(save.getImageModels().get(0).getIid());
        productModelRepository.save(save);
        return productModelDTOUtil.toDTO(model);
    }
    private ImageModel toAddImageModelForProduct(ProductModel productModel, MultipartFile file) throws IOException {
        ImageModel imageModel = new ImageModel();
        if(file.getSize()!=0){
            imageModel = toImageEntity(file);
            productModel.addImageToProduct(imageModel);
        }
        return imageModel;
    }

    private ImageModel toImageEntity(MultipartFile image1) throws IOException {
        return ImageModel
                .builder()
                .name(image1.getName())
                .size(image1.getSize())
                .bytes(image1.getBytes())
                .contentType(image1.getContentType())
                .originalFileName(image1.getOriginalFilename())
                .build();
    }


    public ProductModelDTO setProductModel(ProductModel productModel, ProductModelDTO productModelDTO) {
        productModel.setAuthor(productModelDTO.getAuthor());
        productModel.setTitle(productModelDTO.getTitle());
        productModel.setCity(productModelDTO.getCity());
        productModel.setDate(productModelDTO.getDate());
        productModel.setPrice(productModelDTO.getPrice());
        productModel.setImage(productModelDTO.getImage());
        productModel.setUrl(productModelDTO.getUrl());
        productModel.setDescription(productModelDTO.getDescription());
        productModelRepository.save(productModel);
        return productModelDTOUtil.toDTO(productModel);
    }

    private ProductModel findProductModelByProductModelId(String id) {
        return productModelRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("This product not found!"));
    }
}
