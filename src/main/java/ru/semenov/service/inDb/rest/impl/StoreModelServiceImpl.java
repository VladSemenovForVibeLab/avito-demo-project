package ru.semenov.service.inDb.rest.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.semenov.model.inDb.StoreModel;
import ru.semenov.model.inDb.UserModel;
import ru.semenov.repository.StoreModelRepository;
import ru.semenov.service.inDb.rest.interf.IStoreModelService;
import ru.semenov.service.inDb.rest.interf.IUserService;

import java.security.Principal;

@Service
public class StoreModelServiceImpl implements IStoreModelService {
    private final StoreModelRepository storeModelRepository;
    private final IUserService userService;

    public StoreModelServiceImpl(StoreModelRepository storeModelRepository, IUserService userService) {
        this.storeModelRepository = storeModelRepository;
        this.userService = userService;
    }

    @Override
    public Page<StoreModel> findAllByActive(boolean active, Pageable pageable) {
        return storeModelRepository.findAllByActive(active, pageable);
    }

    @Override
    public Page<StoreModel> findAllByTag(String tag, Pageable pageable) {
        return storeModelRepository.findAllByTag(tag, pageable);
    }

    @Override
    public Page<StoreModel> findAllByTagAndActive(String tag, boolean active, Pageable pageable) {
        return storeModelRepository.findAllByTagAndActive(tag, active, pageable);
    }

    @Override
    public Page<StoreModel> findAllByNameContaining(String name, Pageable pageable) {
        return storeModelRepository.findAllByNameContaining(name, pageable);
    }

    @Override
    public Page<StoreModel> findAllByNameContainingAndActive(String name, boolean active, Pageable pageable) {
        return storeModelRepository.findAllByNameContainingAndActive(name, active, pageable);
    }

    @Override
    public Page<StoreModel> findAllByLocationContaining(String location, Pageable pageable) {
        return storeModelRepository.findAllByLocationContaining(location, pageable);
    }

    @Override
    public Page<StoreModel> findAllByLocationContainingAndActive(String location, boolean active, Pageable pageable) {
        return storeModelRepository.findAllByLocationContainingAndActive(location, active, pageable);
    }

    @Override
    public Page<StoreModel> findAllByNumberOfProducts(int numberOfProducts, Pageable pageable) {
        return storeModelRepository.findAllByNumberOfProducts(numberOfProducts, pageable);
    }

    @Override
    public Page<StoreModel> findAllByNumberOfProductsAndActive(int numberOfProducts, boolean active, Pageable pageable) {
        return storeModelRepository.findAllByNumberOfProductsAndActive(numberOfProducts, active, pageable);
    }

    @Override
    public Page<StoreModel> findAllByUserModel(Principal userModel, Pageable pageable) {
        UserModel find = userService.getByEmail(userModel.getName());
        return storeModelRepository.findAllByUserModel(find, pageable);
    }
}
