package ru.semenov.service.inDb.rest.interf;

import org.springframework.web.multipart.MultipartFile;
import ru.semenov.dto.imageModel.ImageModelDTO;
import ru.semenov.dto.productModel.ProductModelDTO;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

public interface IProductModelService {
    ProductModelDTO create(ProductModelDTO productModelDTO, Principal principal) throws IOException;
    ProductModelDTO read(String id);
    ProductModelDTO update(ProductModelDTO productModelDTO,String id);
    void delete(String id);
    List<ProductModelDTO> findAll();

    ProductModelDTO findByTitle(String title);
    ProductModelDTO saveImageForProductModel(MultipartFile image1,MultipartFile image2,MultipartFile image3, String id) throws IOException;
}
