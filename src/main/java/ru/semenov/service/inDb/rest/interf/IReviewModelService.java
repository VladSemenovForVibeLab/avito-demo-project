package ru.semenov.service.inDb.rest.interf;

import org.springframework.data.domain.Pageable;
import ru.semenov.dto.record.review.ReviewDTO;

import java.util.List;

public interface IReviewModelService {
    List<ReviewDTO> getAllByComment(String comment, Pageable pageable);
    List<ReviewDTO> getAllByRating(int rating, Pageable pageable);
    List<ReviewDTO> getAllByProduct(String productId, Pageable pageable);
}
