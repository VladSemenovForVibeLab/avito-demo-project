package ru.semenov.service.inDb.rest.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.semenov.model.inDb.CachePOJO;
import ru.semenov.service.inDb.rest.interf.IHighloadService;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class HighloadServiceImpl implements IHighloadService {
    @Override
    @Cacheable(cacheNames = {"cachePOJO"},key = "#id")
    public CachePOJO getById(String id) {
        try {
            TimeUnit.SECONDS.sleep(10);
            return new CachePOJO(id, LocalDateTime.now());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @CachePut(cacheNames = {"cachePOJO"},key = "#id")
    public CachePOJO createOrUpdate(String id) {
        return new CachePOJO(id, LocalDateTime.now());
    }

    @Override
    @CacheEvict(cacheNames = {"cachePOJO"},key = "#id")
    public void delete(String id) {
        log.info("delete id: " + id);
    }
}
