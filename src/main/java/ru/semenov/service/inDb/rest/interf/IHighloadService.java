package ru.semenov.service.inDb.rest.interf;

import ru.semenov.model.inDb.CachePOJO;

public interface IHighloadService {
    CachePOJO getById(String id);
    CachePOJO createOrUpdate(String id);
    void delete(String id);
}
