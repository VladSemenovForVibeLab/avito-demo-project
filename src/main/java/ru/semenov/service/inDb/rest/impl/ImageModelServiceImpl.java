package ru.semenov.service.inDb.rest.impl;

import org.springframework.stereotype.Service;
import ru.semenov.dto.imageModel.ImageModelDTO;
import ru.semenov.model.exception.ResourceNotFoundException;
import ru.semenov.repository.ImageModelRepository;
import ru.semenov.service.inDb.rest.interf.IImageModelService;
import ru.semenov.util.inDb.ImageModelDTOUtil;

import java.util.List;

@Service
public class ImageModelServiceImpl implements IImageModelService {
    private final ImageModelRepository imageModelRepository;
    private final ImageModelDTOUtil imageModelDTOUtil;

    public ImageModelServiceImpl(ImageModelRepository imageModelRepository, ImageModelDTOUtil imageModelDTOUtil) {
        this.imageModelRepository = imageModelRepository;
        this.imageModelDTOUtil = imageModelDTOUtil;
    }

    @Override
    public ImageModelDTO findImageModelById(String id) {
        return imageModelDTOUtil.toDTO(
                imageModelRepository.findById(id).orElseThrow(
                        (()-> new ResourceNotFoundException(String.format("Image model with id %s not found", id)))));
    }

    @Override
    public List<ImageModelDTO> findAllImageModels() {
        return imageModelDTOUtil.toDTO(imageModelRepository.findAll());
    }
}
