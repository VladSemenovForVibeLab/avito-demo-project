package ru.semenov.service.inDb.rest.interf;

import ru.semenov.dto.imageModel.ImageModelDTO;

import java.util.List;

public interface IImageModelService {
    ImageModelDTO findImageModelById(String id);

    List<ImageModelDTO> findAllImageModels();
}
