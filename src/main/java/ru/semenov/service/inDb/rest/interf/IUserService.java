package ru.semenov.service.inDb.rest.interf;

import ru.semenov.dto.request.JWTRequest;
import ru.semenov.dto.response.JWTResponse;
import ru.semenov.dto.response.MessageResponse;
import ru.semenov.dto.response.UserModelResponse;
import ru.semenov.dto.userModel.UserModelDTORequestForRegister;
import ru.semenov.model.inDb.UserModel;

public interface IUserService {
       boolean createUserModel(UserModelDTORequestForRegister userModelDTORequestForRegister);

       UserModel getByEmail(String email);

       UserModelDTORequestForRegister findById(String userId);
       JWTResponse login(JWTRequest loginRequest);

       UserModelResponse userModelProfile(String id);

       MessageResponse adminProfile(String id);

       MessageResponse adminEnabled(String id);

       MessageResponse adminCredentialsExpired(String uid);

       MessageResponse adminAccountLocked(String uid);

       MessageResponse adminAccountExpired(String uid);


       boolean activateUserModel(String code);

}
