package ru.semenov.service.inDb.rest.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.semenov.dto.productModel.ProductModelDTO;
import ru.semenov.dto.record.review.ReviewDTO;
import ru.semenov.mapper.ReviewDTOMapper;
import ru.semenov.repository.ReviewModelRepository;
import ru.semenov.service.inDb.rest.interf.IProductModelService;
import ru.semenov.service.inDb.rest.interf.IReviewModelService;
import ru.semenov.util.inDb.ProductModelDTOUtil;

import java.util.List;
@Service
@Slf4j
public class ReviewModelServiceImpl implements IReviewModelService {
    private final ReviewModelRepository reviewModelRepository;
    private final ReviewDTOMapper reviewDTOMapper;
    private final IProductModelService iProductModelService;
    private final ProductModelDTOUtil productModelDTOUtil;

    public ReviewModelServiceImpl(ReviewModelRepository reviewModelRepository, ReviewDTOMapper reviewDTOMapper, IProductModelService iProductModelService, ProductModelDTOUtil productModelDTOUtil) {
        this.reviewModelRepository = reviewModelRepository;
        this.reviewDTOMapper = reviewDTOMapper;
        this.iProductModelService = iProductModelService;
        this.productModelDTOUtil = productModelDTOUtil;
    }

    @Override
    public List<ReviewDTO> getAllByComment(String comment, Pageable pageable) {
        return reviewModelRepository.findAllByCommentContaining(comment, pageable)
                .stream()
                .map(reviewDTOMapper)
                .toList();
    }

    @Override
    public List<ReviewDTO> getAllByRating(int rating, Pageable pageable) {
        return reviewModelRepository.findAllByRatingContaining(rating, pageable)
                .stream()
                .map(reviewDTOMapper)
                .toList();
    }


    @Override
    public List<ReviewDTO> getAllByProduct(String productId, Pageable pageable) {
        ProductModelDTO read = iProductModelService.read(productId);
        return reviewModelRepository.findAllByProductModel(productModelDTOUtil.toEntity(read), pageable)
                .stream()
                .map(reviewDTOMapper)
                .toList();
    }

}
