package ru.semenov.service.inDb.rest.interf;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;


public interface IMailSender {
   void sendMail(String to, String subject, String text);
}
