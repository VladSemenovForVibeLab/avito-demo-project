package ru.semenov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class AvitoDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvitoDemoApplication.class, args);
    }
}
