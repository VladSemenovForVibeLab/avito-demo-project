package ru.semenov.controller.inMemory.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.dto.product.request.ProductDTORequest;
import ru.semenov.dto.product.response.ProductDTO;
import ru.semenov.dto.response.MessageResponse;
import ru.semenov.model.inMemory.Product;
import ru.semenov.service.inMemory.interf.IProductService;

import java.util.List;

@RestController
@RequestMapping(ProductRESTController.PRODUCT_REST_URL)
public class ProductRESTController {
    public final static String PRODUCT_REST_URL="/api/v1/rest/products";
    private final IProductService productService;
    public ProductRESTController(IProductService productService) {
        this.productService = productService;
    }
    @GetMapping("/")
    public ResponseEntity<List<Product>> products() {
        return ResponseEntity.ok(productService.list());
    }
    @PostMapping("/create")
    public ResponseEntity<MessageResponse> create(@RequestBody ProductDTO productDTO) {
        productService.saveProduct(productDTO);
        return new ResponseEntity<>(new MessageResponse(HttpStatus.CREATED,"Product Created!"), HttpStatus.OK);
    }
    @GetMapping("/read/{id}")
    public ResponseEntity<ProductDTO> read(@PathVariable Long id) {
        return ResponseEntity.ok((productService.read(id)));
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<MessageResponse> delete(@PathVariable Long id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>(new MessageResponse(HttpStatus.NO_CONTENT,"Product Deleted!"), HttpStatus.OK);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<MessageResponse> update(@RequestBody ProductDTORequest productDTORequest,
                                                  @PathVariable Long id) {
        productService.updateProduct(productDTORequest,id);
        return new ResponseEntity<>(new MessageResponse(HttpStatus.OK,"Product Updated!"), HttpStatus.OK);
    }
}
