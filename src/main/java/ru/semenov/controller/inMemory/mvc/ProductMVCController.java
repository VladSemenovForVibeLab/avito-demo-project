package ru.semenov.controller.inMemory.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.semenov.model.inMemory.Product;
import ru.semenov.service.inMemory.interf.IProductService;

@Controller
@RequestMapping(ProductMVCController.PRODUCT_MVC_URL)
public class ProductMVCController {
    public final static String PRODUCT_MVC_URL="/api/v1/mvc/products";
    private final IProductService productService;

    public ProductMVCController(IProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/")
    public String products(Model model) {
        model.addAttribute("products", productService.list());
        return "products";
    }
    @PostMapping("/create")
   public String createProduct(Product product){
        productService.saveProduct(product);
        return "redirect:/api/v1/mvc/products/";
   }
   @PostMapping("/delete/{id}")
    public String deleteProduct(@PathVariable Long id){
        productService.deleteProduct(id);
        return "redirect:/api/v1/mvc/products/";
    }
    @GetMapping("/{id}")
    public String getProduct(@PathVariable Long id, Model model){
        model.addAttribute("product", productService.getProductById(id));
        return "product-info";
    }
}
