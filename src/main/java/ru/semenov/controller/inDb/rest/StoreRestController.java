package ru.semenov.controller.inDb.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ru.semenov.model.inDb.StoreModel;
import ru.semenov.service.inDb.rest.interf.IStoreModelService;

import java.security.Principal;

@RestController
@RequestMapping(StoreRestController.STORE_MODEL_URL)
public class StoreRestController {
    public static final String STORE_MODEL_URL = "/api/v1/rest/store-models";
    private final IStoreModelService storeModelService;

    public StoreRestController(IStoreModelService storeModelService) {
        this.storeModelService = storeModelService;
    }

    @GetMapping("/allByActive/{active}")
    public Page<StoreModel> getAll(@PathVariable boolean active, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return storeModelService.findAllByActive(active, pageable);
    }
    @GetMapping("/allByTag/{tag}")
    public Page<StoreModel> getAllByTag(@PathVariable String tag, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return storeModelService.findAllByTag(tag, pageable);
    }
    @GetMapping("/allByTagAndActive/{tag}/{active}")
    public Page<StoreModel> getAllByTagAndActive(@PathVariable String tag, @PathVariable boolean active, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return storeModelService.findAllByTagAndActive(tag, active, pageable);
    }
    @GetMapping("/allByName/{name}")
    public Page<StoreModel> getAllByNameContaining(@PathVariable String name, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return storeModelService.findAllByNameContaining(name, pageable);
    }
    @GetMapping("/allByNameAndActive/{name}/{active}")
    public Page<StoreModel> getAllByNameContainingAndActive(@PathVariable String name, @PathVariable boolean active, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return storeModelService.findAllByNameContainingAndActive(name, active, pageable);
    }
    @GetMapping("/allByLocation/{location}")
    public Page<StoreModel> getAllByLocationContaining(@PathVariable String location, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return storeModelService.findAllByLocationContaining(location, pageable);
    }
    @GetMapping("/allByLocationAndActive/{location}/{active}")
    public Page<StoreModel> getAllByLocationContainingAndActive(@PathVariable String location, @PathVariable boolean active, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return storeModelService.findAllByLocationContainingAndActive(location, active, pageable);
    }
    @GetMapping("/allByNumberOfProducts/{numberOfProducts}")
    public Page<StoreModel> getAllByNumberOfProducts(@PathVariable int numberOfProducts, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return storeModelService.findAllByNumberOfProducts(numberOfProducts, pageable);
    }
    @GetMapping("/allByNumberOfProductsAndActive/{numberOfProducts}/{active}")
    public Page<StoreModel> getAllByNumberOfProductsAndActive(@PathVariable int numberOfProducts, @PathVariable boolean active, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return storeModelService.findAllByNumberOfProductsAndActive(numberOfProducts, active, pageable);
    }
    @GetMapping("/allByUserModel")
    public Page<StoreModel> getAllByUserModel(Principal principal, @PageableDefault(sort = {"sid"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return storeModelService.findAllByUserModel(principal,pageable);
    }


}
