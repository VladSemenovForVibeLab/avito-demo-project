package ru.semenov.controller.inDb.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.semenov.dto.productModel.ProductModelDTO;
import ru.semenov.dto.response.MessageResponse;
import ru.semenov.service.inDb.rest.interf.IProductModelService;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(ProductModelRestController.PRODUCT_MODEL_PATH)
public class ProductModelRestController {
    public static final String PRODUCT_MODEL_PATH = "/api/v1/rest/productModel";
    private final IProductModelService productModelService;

    public ProductModelRestController(IProductModelService productModelService) {
        this.productModelService = productModelService;
    }
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ProductModelDTO>> findAll() {
        return new ResponseEntity<>(productModelService.findAll(), HttpStatus.OK);
    }
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ProductModelDTO> create(@RequestBody ProductModelDTO productModelDTO,
                                                  Principal principal) throws IOException {
        return new ResponseEntity<>(productModelService.create(productModelDTO,principal), HttpStatus.OK);
    }
    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProductModelDTO> read(@PathVariable String id) {
        return new ResponseEntity<>(productModelService.read(id), HttpStatus.OK);
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ProductModelDTO> update(@PathVariable String id, ProductModelDTO productModelDTO) {
        return new ResponseEntity<>(productModelService.update(productModelDTO,id), HttpStatus.OK);
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<MessageResponse> delete(@PathVariable String id) {
        productModelService.delete(id);
        return new ResponseEntity<>(new MessageResponse(HttpStatus.NO_CONTENT,"DELETED SUCCESS"), HttpStatus.OK);
    }
    @GetMapping("/title/{title}")
    public ResponseEntity<ProductModelDTO> findByTitle(@PathVariable String title){
        return new ResponseEntity<>(productModelService.findByTitle(title),HttpStatus.OK);
    }
    @PostMapping("/image")
    public ResponseEntity<ProductModelDTO> saveImageForProductModel(@RequestParam("image1") MultipartFile image1, @RequestParam("image2") MultipartFile image2, @RequestParam("image3") MultipartFile image3, @RequestParam("id") String id) throws IOException {
        return new ResponseEntity<>(productModelService.saveImageForProductModel(image1,image2,image3,id),HttpStatus.OK);
    }

}
