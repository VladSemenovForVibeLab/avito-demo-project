package ru.semenov.controller.inDb.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.model.inDb.CachePOJO;
import ru.semenov.service.inDb.rest.interf.IHighloadService;

@RestController
@RequestMapping(HighloadController.HIGHLOAD_URL)
public class HighloadController {
    public static final String HIGHLOAD_URL = "/api/v1/rest/cache/highload";
    private final IHighloadService highloadService;

    public HighloadController(IHighloadService highloadService) {
        this.highloadService = highloadService;
    }
    @GetMapping("/{id}")
    public ResponseEntity<CachePOJO> getById(@PathVariable String id) {
        return new ResponseEntity<>(highloadService.getById(id), HttpStatus.OK);
    }
    @PutMapping("/{id}/update")
    public ResponseEntity<CachePOJO> createOrUpdate(@PathVariable String id) {
        return new ResponseEntity<>(highloadService.createOrUpdate(id), HttpStatus.OK);
    }
    @DeleteMapping("/{id}/delete")
    public void delete(@PathVariable String id) {
        highloadService.delete(id);
    }
}
