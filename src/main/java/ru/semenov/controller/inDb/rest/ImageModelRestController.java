package ru.semenov.controller.inDb.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.dto.imageModel.ImageModelDTO;
import ru.semenov.repository.ImageModelRepository;
import ru.semenov.service.inDb.rest.impl.ImageModelServiceImpl;
import ru.semenov.service.inDb.rest.interf.IImageModelService;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.List;

@RestController
@RequestMapping(ImageModelRestController.IMAGE_REST_URL)
public class ImageModelRestController {
    public static final String IMAGE_REST_URL="api/v1/rest/images";
    private final IImageModelService iImageModelService;

    public ImageModelRestController(IImageModelService iImageModelService) {
        this.iImageModelService = iImageModelService;
    }


    @GetMapping("/{id}")
    private ResponseEntity<?> getImageModelById(String id){
        ImageModelDTO imageModelById = iImageModelService.findImageModelById(id);
        return ResponseEntity.ok()
                .header("fileName",imageModelById.getOriginalFileName())
                .contentType(MediaType.valueOf(imageModelById.getContentType()))
                .contentLength(imageModelById.getSize())
                .body(new InputStreamReader(new ByteArrayInputStream(imageModelById.getBytes())));
    }
    @GetMapping("/all")
    private ResponseEntity<?> getAllImageModels(){
        List<ImageModelDTO> imageModels = iImageModelService.findAllImageModels();
        return ResponseEntity.ok(imageModels);
    }
}
