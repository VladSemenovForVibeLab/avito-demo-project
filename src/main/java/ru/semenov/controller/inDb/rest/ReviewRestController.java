package ru.semenov.controller.inDb.rest;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.dto.record.review.ReviewDTO;
import ru.semenov.service.inDb.rest.interf.IReviewModelService;

import java.util.List;

@RestController
@RequestMapping(ReviewRestController.REVIEW_REST_URL)
public class ReviewRestController {
    public static final String REVIEW_REST_URL ="/api/v1/rest/reviews";
    private final IReviewModelService iReviewModelService;

    public ReviewRestController(IReviewModelService iReviewModelService) {
        this.iReviewModelService = iReviewModelService;
    }

    @GetMapping("/reviews-by-comment/{comment}")
    public ResponseEntity<List<ReviewDTO>> getReviewByComment(@PathVariable("comment") String comment,
                                                              @PageableDefault(sort = {"comment"}, direction = Sort.Direction.ASC)Pageable pageable) {
        return new ResponseEntity<>(iReviewModelService.getAllByComment(comment, pageable),
                HttpStatus.OK);
    }
    @PostMapping("/reviews-by-rating")
    public ResponseEntity<List<ReviewDTO>> getAllByRating(@RequestBody Integer rating,
                                                          @PageableDefault(sort = {"rating"}, direction = Sort.Direction.DESC)Pageable pageable) {
        return new ResponseEntity<>(iReviewModelService.getAllByRating(rating, pageable),
                HttpStatus.OK);
    }

    @GetMapping("/reviews-by-product/{productId}")
    public ResponseEntity<List<ReviewDTO>> getAllByUser(@PathVariable("productId") String productId,
                                                        @PageableDefault(sort = {"reviewId"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return new ResponseEntity<>(iReviewModelService.getAllByProduct(productId, pageable),
                HttpStatus.FOUND);
    }
}
