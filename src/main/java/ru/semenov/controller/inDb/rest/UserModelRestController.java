package ru.semenov.controller.inDb.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.semenov.dto.request.JWTRequest;
import ru.semenov.dto.response.JWTResponse;
import ru.semenov.dto.response.MessageResponse;
import ru.semenov.dto.response.UserModelResponse;
import ru.semenov.dto.userModel.UserModelDTORequestForRegister;
import ru.semenov.service.inDb.rest.interf.IUserService;

@RestController
@RequestMapping(UserModelRestController.USER_MODEL_URL)
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class UserModelRestController {
    private final IUserService iUserService;
    public static final String USER_MODEL_URL = "/api/v1/rest/users";

    @PostMapping("/register")
    public ResponseEntity<MessageResponse> register(@RequestBody UserModelDTORequestForRegister userModelDTORequestForRegister) {
        iUserService.createUserModel(userModelDTORequestForRegister);
        return new ResponseEntity<>(new MessageResponse(HttpStatus.CREATED,"User created" ),HttpStatus.OK);
    }
    @PostMapping("/login")
    public JWTResponse login(@Validated @RequestBody JWTRequest loginRequest){
        return iUserService.login(loginRequest);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserModelResponse> userModelProfile(@PathVariable String id){
        return new ResponseEntity<>(iUserService.userModelProfile(id),HttpStatus.OK);
    }
    @GetMapping("/activate/{code}")
    public ResponseEntity<MessageResponse> activate(@PathVariable String code){
       boolean isActivated = iUserService.activateUserModel(code);
       if(isActivated) {
           return new ResponseEntity<>(new MessageResponse(HttpStatus.OK,"User activated"),HttpStatus.OK);
       }
        return new ResponseEntity<>(new MessageResponse(HttpStatus.BAD_REQUEST,"User not activated"),HttpStatus.BAD_REQUEST);
    }

}
