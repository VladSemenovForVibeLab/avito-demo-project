package ru.semenov.controller.inDb.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.semenov.model.exception.ExceptionBody;
import ru.semenov.model.exception.ResourceMappingException;
import ru.semenov.model.exception.ResourceNotFoundException;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;

@RestControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionBody handleResourceNotFoundException(ResourceNotFoundException e) {
        return new ExceptionBody("This model not found","404",HttpStatus.NOT_FOUND.toString(),e.getLocalizedMessage(), LocalDateTime.now().toString(),e.getStackTrace().toString());
    }
    @ExceptionHandler(ResourceMappingException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionBody handleResourceMapping(ResourceMappingException e) {
        return new ExceptionBody("This model not mapping","500",HttpStatus.INTERNAL_SERVER_ERROR.toString(),e.getLocalizedMessage(), LocalDateTime.now().toString(),e.getStackTrace().toString());
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleIllegalState(IllegalStateException e) {
        return new ExceptionBody("Illegal state exception","400",HttpStatus.BAD_REQUEST.toString(),e.getLocalizedMessage(), LocalDateTime.now().toString(),e.getStackTrace().toString());
    }
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleSQLIntegrityConstraintViolation(SQLIntegrityConstraintViolationException e) {
        return new ExceptionBody("SQL integrity exception -> bad request param","400",HttpStatus.BAD_REQUEST.toString(),e.getLocalizedMessage(), LocalDateTime.now().toString(),e.getStackTrace().toString());
    }
}

