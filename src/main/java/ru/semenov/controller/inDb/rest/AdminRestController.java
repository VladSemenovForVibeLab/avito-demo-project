package ru.semenov.controller.inDb.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.dto.response.MessageResponse;
import ru.semenov.service.inDb.rest.interf.IUserService;

@RestController
@RequestMapping(AdminRestController.ADMIN_URL)
//@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminRestController {
    public static final String ADMIN_URL = "/api/v1/rest/admin";
    private final IUserService iUserService;

    public AdminRestController(IUserService iUserService) {
        this.iUserService = iUserService;
    }
    @PostMapping("/{id}")
    public ResponseEntity<MessageResponse> adminProfile(@PathVariable String id){
        return new ResponseEntity<>(iUserService.adminProfile(id), HttpStatus.OK);
    }
    @PatchMapping("/enabled/{id}")
    public ResponseEntity<MessageResponse> adminEnabled(@PathVariable String id){
        return new ResponseEntity<>(iUserService.adminEnabled(id), HttpStatus.OK);
    }
    @PatchMapping("/credentials/expired/{uid}")
    public ResponseEntity<MessageResponse> adminCredentialsExpired(@PathVariable String uid){
        return new ResponseEntity<>(iUserService.adminCredentialsExpired(uid), HttpStatus.OK);
    }
    @PatchMapping("/account/locked/{uid}")
    public ResponseEntity<MessageResponse> adminAccountLocked(@PathVariable String uid){
        return new ResponseEntity<>(iUserService.adminAccountLocked(uid), HttpStatus.OK);
    }
    @PatchMapping("/account/expired/{uid}")
    public ResponseEntity<MessageResponse> adminAccountExpired(@PathVariable String uid){
        return new ResponseEntity<>(iUserService.adminAccountExpired(uid), HttpStatus.OK);
    }
}
