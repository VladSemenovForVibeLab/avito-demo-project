package ru.semenov.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;
/**
 * Configuration class for email services.
 */

@Configuration
public class EmailConfiguration {
    /**
     * Email username.
     */
    @Value("${spring.mail.username}")
    private String username;

    /**
     * Email password.
     */
    @Value("${spring.mail.password}")
    private String password;
    /**
     * Email host.
     */
    @Value("${spring.mail.host}")
    private String host;
    /**
     * Email port.
     */
    @Value("${spring.mail.port}")
    private int port;
    /**
     * Email protocol.
     */
    @Value("${spring.mail.protocol}")
    private String protocol;
    /**
     * Email debug mode.
     */
    @Value("${mail.debug}")
    private String debug;

    /**
     * Returns a JavaMailSender bean that can be used to send email.
     *
     * @return a JavaMailSender bean
     */

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);

        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", protocol);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", debug);

        return mailSender;
    }
}
