# Демо-приложение на Spring Boot - AVITO DEMO 

Данное демо-приложение является примером использования Spring Boot для создания RESTful API с использованием множества
технологий, таких как MySQL, Spring Data JPA, Spring Security, springdoc OpenAPI, Lombok и многих других.

## Технологии

- Java 17
- Spring Boot 3.2.1
- springdoc OpenAPI 2.1.0
- Spring Data JPA
- Spring Data REST
- Spring Freemarker
- Spring Mail
- Spring Security
- Spring Web
- Spring DevTools
- MySQL
- Lombok
- Spring Test
- Spring Security JWT
- Maven

## Установка и запуск

1. Клонировать репозиторий:

bash
```
git clone https://gitlab.com/VladSemenovForVibeLab/avito-demo-project.git
```

2. В Intellij IDEA или Eclipse, открыть проект как maven-проект.

3. Настроить MySQL базу данных в файле `application.properties`:

properties
```
spring.datasource.url=jdbc:mysql://localhost:3306/yourdatabase
spring.datasource.username=dbusername
spring.datasource.password=dbpassword
```

4. Запустить приложение из вашей IDE или выполнить следующую команду в терминале:

bash
```
mvn spring-boot:run
```

5. Перейти по адресу `http://localhost:8083`, где вы сможете использовать RESTful API.

## Документация API

Документация API сгенерирована автоматически с помощью springdoc OpenAPI и доступна по адресу:

`http://localhost:8083/v3/api-docs` - JSON документация OpenAPI

`http://localhost:8083/swagger-ui/index.html#/` - Swagger UI для взаимодействия с API

## Развертывание на сервере

Для развертывания приложения на сервере, сначала убедитесь, что сервер соответствует требованиям приложения (например,
установлен JDK 17, MySQL). Затем сконфигурируйте `application.properties` для подключения к вашей серверной базе данных.
Затем, соберите проект и разверните его на сервере.

## Contributing

Если вы хотите внести свой вклад в данное демо-приложение, смело создавайте Issue или Pull Request в репозитории.
